module anirudhanr.com/elisity/inventory

go 1.16

require (
	github.com/aws/aws-lambda-go v1.24.0 // indirect
	github.com/aws/aws-sdk-go-v2 v1.7.0 // indirect
	github.com/aws/aws-sdk-go-v2/config v1.4.0 // indirect
	github.com/awslabs/aws-lambda-go-api-proxy v0.10.0 // indirect
	github.com/gin-gonic/gin v1.7.2 // indirect
)
