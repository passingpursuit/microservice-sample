package main

import (
	"log"

	"anirudhanr.com/elisity/inventory/router"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	router.LoadRoutes(r)
	// listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
	if err := r.Run(); err != nil {
		log.Printf("error starting server %+v", err)
	}
}
