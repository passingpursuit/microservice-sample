terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.47"
        }
    }
    required_version = "1.0.1"
}

variable "aws_profile" {
    type = string
    description = "Your AWS profile name.  This corresponds to credentials in your aws cli"
}

variable "aws_region" {
  type = string
  description = "AWS region to deploy"
}

locals {
  inventoryService = "InventoryService"
  configurationService = "ConfigurationService"
  commonComponent = "Common"

  common_tags = {
    Environment = "production"
    Reason  = "elisity-assignment"
  }
}


provider "aws" {
    region = var.aws_region
    profile = var.aws_profile
}

resource "aws_iam_role" "inventoryServiceRole" {
    name = "inventoryService"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
    EOF
}

resource "aws_iam_role" "configurationServiceRole" {
    name = "configurationService"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
    EOF
}

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "inventoryPolicyAttachment" {
  role       = aws_iam_role.inventoryServiceRole.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

resource "aws_iam_role_policy_attachment" "configurationPolicyAttachment" {
  role       = aws_iam_role.configurationServiceRole.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

resource "aws_cloudwatch_log_group" "inventoryLogGroup" {
  name              = "/aws/lambda/inventoryService"
  retention_in_days = 14
}

resource "aws_cloudwatch_log_group" "configurationLogGroup" {
  name              = "/aws/lambda/configurationService"
  retention_in_days = 14
}

resource "aws_s3_bucket" "deployArtifactory" {
    bucket = "anirudhanr-artifactory-bucket"
    versioning {
      enabled = true
    }
    tags = merge(local.common_tags, tomap({
        Component = local.commonComponent
    }))
}

data "archive_file" "inventoryArtifact" {
  type        = "zip"
  source_file = "../inventoryService/main"
  output_path = "../build/inventoryService.zip"
}

data "archive_file" "configurationArtifact" {
  type        = "zip"
  source_file = "../configurationService/main.out"
  output_path = "../build/configurationService.zip"
}


resource "aws_s3_bucket_object" "inventoryDeploymentPackage" {
  bucket = aws_s3_bucket.deployArtifactory.bucket
  key = "inventoryartifact/inventoryService.zip"
  source = data.archive_file.inventoryArtifact.output_path
  tags = merge(local.common_tags, tomap({
        Component = local.inventoryService
    }))
}

resource "aws_s3_bucket_object" "configurationDeploymentPackage" {
  bucket = aws_s3_bucket.deployArtifactory.bucket
  key = "configurationArtifact/configService.zip"
  source = data.archive_file.configurationArtifact.output_path
  tags = merge(local.common_tags, tomap({
        Component = local.configurationService
    }))
}

resource "aws_lambda_function" "inventoryService" {
    function_name = "InventoryService"
    role = aws_iam_role.inventoryServiceRole.arn
    s3_bucket = aws_s3_bucket.deployArtifactory.bucket
    s3_key = aws_s3_bucket_object.inventoryDeploymentPackage.key
    source_code_hash = data.archive_file.inventoryArtifact.output_base64sha256
    handler = "main"
    runtime = "go1.x"
    layers = [ ]

    tags = merge(local.common_tags, tomap({
        Component = local.inventoryService
    }))

    depends_on = [
        aws_iam_role_policy_attachment.inventoryPolicyAttachment,
        aws_cloudwatch_log_group.inventoryLogGroup,
    ]
}

resource "aws_lambda_function" "configurationService" {
    function_name = "ConfigurationService"
    role = aws_iam_role.configurationServiceRole.arn
    s3_bucket = aws_s3_bucket.deployArtifactory.bucket
    s3_key = aws_s3_bucket_object.configurationDeploymentPackage.key
    source_code_hash = data.archive_file.configurationArtifact.output_base64sha256
    handler = "main"
    runtime = "go1.x"
    layers = [ ]

    tags = merge(local.common_tags, tomap({
        Component = local.configurationService
    }))

    depends_on = [
        aws_iam_role_policy_attachment.configurationPolicyAttachment,
        aws_cloudwatch_log_group.configurationLogGroup,
    ]
}
