module anirudhanr.com/elisity/configuration

go 1.16

require (
	github.com/aws/aws-lambda-go v1.24.0 // indirect
	github.com/aws/aws-sdk-go-v2 v1.7.0 // indirect
	github.com/aws/aws-sdk-go-v2/config v1.4.0 // indirect
)
